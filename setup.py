from setuptools import setup

setup(
    name = "stvalidate",
    packages = ["stvalidate"],
    scripts=["scripts/stvalidate"],
    zip_safe = False,
    )